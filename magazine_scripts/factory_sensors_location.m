% Function generate randomly drone stops
function [matrix_stop_positions] = ...
    factory_sensors_location(num_stops,...
    initial_point,...
    final_point,...
    method)
if method == '1'
    % equal__separated
    x_values = linspace(initial_point, final_point, num_stops);
    y_values = zeros(1,length(x_values));
    z_values = zeros(1,length(x_values));
    
    matrix_stop_positions = [x_values;... %x
        y_values;... %y
        z_values];  %z
    
elseif method == '2'
    radio = 0.1;
    numb_extra_nodes = 2;
    % equal__separated_cluster
    % ############ To be developed  ########### %z
    x_values = linspace(initial_point, final_point, num_stops);
    x_value_new = [];
    for ii = 1:length(x_values)
        vector_temp = [];
        for numb_extra_nodes_index = 0:numb_extra_nodes-1
            if mod(numb_extra_nodes_index,2)==0 % if PAR
                vector_temp = [vector_temp x_values(ii)+radio*numb_extra_nodes_index/2];
            else %if IMPAR
                vector_temp = [ x_values(ii)-radio*(numb_extra_nodes_index+1)/2 vector_temp];
            end
        end
        x_value_new = [ x_value_new vector_temp];
   end
    y_values = zeros(1,length(x_value_new));
    z_values = zeros(1,length(x_value_new));
    
    matrix_stop_positions = [x_value_new;... %x
        y_values;... %y
        z_values];  %z
elseif method == '3'
    % random_separated
    % based on initial_point and final_point choose randomly the
    % positions of the num_stops
    % only for x axis in this version
    x_values = [initial_point sort(floor(...
        (final_point-initial_point)*rand(1,num_stops-2)+initial_point))...
        final_point];
    y_values = zeros(1,length(x_values));
    z_values = zeros(1,length(x_values));
    
    matrix_stop_positions = [x_values;... %x
        y_values;... %y
        z_values];  %z
    
elseif method == '4'
    % random_separated_cluster
    % To be developed  %z
    
else
    % This scenario is not necessary for Drone Stops
    Display('You choosed an unsupported scenario...')
    
    
end
end