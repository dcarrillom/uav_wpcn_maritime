% High level simulation UAVs + WPT + sensor clusters
% Authors: Konstantin Mikhaylov, Dick Carrillo Melgarejo
% ======================================================================
% tip
% Run first run with Cluster: option 1- option 2
% Next run without Cluster: option 2 - option 1 (change plot only for "-")
% Next, activate ZOOM with Cluster: option 1 - option 2

clear all
close all
%for input_time_stop_duration_s = [16 19.5 20 25 27 70] %in seconds
only_once =0;
lineplot_vec=['-.';'- ';'-.'];
plot_zoom_vec=[0;0;1];
param_vec = [2;1;2];
C =colororder;
for jj=1:3
    display('hola')
    for input_time_stop_duration_s = [ 16 20 25 70] %in seconds
        number_stops = [];
        sum_packetsTx = [];
        numSensorsTxpackages_vector = [];
        eff_tx_packages_vector = [];
        vec_effi_stops =[];
        for numstops = 10:80
            %SENSORS
            %implications
            %implication 1: UAV uses a sort of wake up radio solution to request data
            %from sensors thus we have TDMA-like MAC and there are no collisions
            %implication 2: sensors do not use any transmit power control (i.e., all use fixed TX power)
            %positions
            %implication 3: the number of sensors, their locations and packet transmit duration are
            %such, that all the packets can be delivered within the designated
            %transmit windows (CURRENTLY THIS IS NOT CHECKED!)
            %implication 4: energy-neutral operation - all available energy is used by
            %sensors for sensing and reporting
            %implication 5: the directivity of sensor's antenna is at least as good as
            %that of the UAV
            %implication 6: UAV/sensor antennas have equal directivity angle and are pointed towards each other.
            %Circular polarization.
            %%% #################### Sensor Positions %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%


                input_coord_sensor_xyz_m = ...
                factory_sensors_location(8,0,25.5,num2str(param_vec(jj)));


            %energy
            input_energy_sensor_buffer_J=[0 0 0 0 0]%amount of energy in the energy buffer of sensors
            input_energy_sensor_buffer_J=zeros(1, length(input_coord_sensor_xyz_m))
            input_energy_sensor_sensing_J=0.01%consumption for a single sensing event - TBD*
            input_energy_sensor_report_J=0.01%consumption for a single communication event - TBD*
            input_efficiency_sensor_charge_percent=72%efficiency of converting energy from RF signal to battery - %https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            input_sensitivity_dBm=0%minimum power level enabling energy harvesting
            %data [Dick Comment] usually at High SNR regime, maybe 15 dBm is a good value
            input_communication_max_distance_m=25 %- TBD* % We can do something similar than line 24 (minimum level for communication)
            
            %UAV
            %implications:
            %implication 1: UAV charges/communicates to sensors only at a stop
            %implication 2: acceleration/deacceleration consumption/time are not considered
            %positions and mobility
            input_coord_UAV_start=[0;0;0]%x;y;z
            input_coord_UAV_end=[30;0;0]%x;y;z
            %note: at least one stop has to be present
            input_coord_stop_xyz_m=  [0 5 10  25;%x
                1 1 1  1;%y
                0 0 0  0]%z
            %note: main antenna direction
            input_stop_WPT_antenna_direction_vector_xyz_m=[ 0 0  0  0;%x
                -1 -1	-1	-1;%y
                0  0	0	0]%z
            
            
            [input_coord_stop_xyz_m,input_stop_WPT_antenna_direction_vector_xyz_m]=...
                factory_drone_stops(numstops,0,25,'equal__separated');
            %  factory_drone_stops(numstops,0,25,'equal__separated');
            %  factory_drone_stops(numstops,0,25,'random_separated');
            
            %[input_coord_stop_xyz_m,input_stop_WPT_antenna_direction_vector_xyz_m]=...
            %    factory_drone_stops(numstops,0,25,'random_separated');
            
            
            input_speed_UAV_mps=10%while moving between stops %https://www.dji.com/fi/phantom-4/info - half of the max
            %input_time_stop_duration_s=15%time spent by UAV at each stop
            %energy-related parameters
            input_power_UAV_receive_constant_W=0.0%constant consumption during stop
            input_energy_UAV_per_event_J=0.01%additional consumption per received packet (e.g., wake-up signal issue)
            input_power_UAV_charge_radiated_W=1.25 %https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            input_power_UAV_charge_radiated_dBm=pow2db(input_power_UAV_charge_radiated_W*1000);
            input_efficiency_UAV_charge_percent=1.25/2.75*100%efficiency of UAV charging - https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            input_energy_UAV_buffer_volume_mWh=5300*15%https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            input_energy_UAV_buffer_volume_J=input_energy_UAV_buffer_volume_mWh*60*60/1000
            input_power_UAV_fly_W=input_energy_UAV_buffer_volume_J/(28*60)% =battery volume/lifetime - https://www.dji.com/fi/phantom-4/info
            input_power_UAV_hower_W=input_power_UAV_fly_W
            input_directivity_UAV_antenna_WPT_degr=140%
            
            %OTHER
            input_frequency_Hz=2.3*10^9;%https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            input_gain_TX_dBi=9.3;%https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            input_gain_RX_dBi=8;%https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
            %dipole: 2.15 dBi
            
            
            %Testcases (to check the feasibility of results)
            %Testcase 1: first two stops are right on top of the sensors at 1 meter distance,
            %3rd stop - 2 sensors at 30 degree angle, 4th stop - 2 sensors at 45
            %degree, antenna direction - sideways
            %angle
            %Expectations:
            %attenuation for first 2 sensors is https://www.everythingrf.com/rf-calculators/free-space-path-loss-calculator
            %aprox 22.37 dB, the TX power is 30.96 dBm, the efficiency of charging is
            %72% and the duration of charging is 100s thus the energy income for 2
            %first sensors should be 30.96-22.37=8.59dBm->
            %Energy=0.0072W*0.72*100s=0.51J
            %for 3rd and 4th sensors the distance is 2/sqrt(3)=1.154m, attenuation is
            %23.61 dB and thus received signal is 30.96-23.61=7.35dBm->
            %Energy=0.0054W*0.72*100s=0.3888J
            %for 5th and 6th sensors - no energy income
            %Result: ok
            if 0
                input_time_stop_duration_s=100
                input_directivity_UAV_antenna_WPT_degr=60%
                input_power_UAV_charge_radiated_W=1.25 %https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
                input_power_UAV_charge_radiated_dBm=pow2db(input_power_UAV_charge_radiated_W*1000);
                input_efficiency_sensor_charge_percent=72%efficiency of converting energy from RF signal to battery - %https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7953846
                
                clearvars input_coord_sensor_xyz_m input_energy_sensor_buffer_J
                input_coord_sensor_xyz_m=[
                    0	10	19.43	20.57	29	31;%x
                    0	0	0       0       0	0;%y
                    0	0	0       0       0	0]%z
                input_energy_sensor_buffer_J=[0 0 0 0 0 0]
                
                clearvars input_coord_UAV_start input_coord_UAV_end input_stop_WPT_antenna_direction_vector_xyz_m
                input_coord_UAV_start=[10;10;10]
                input_coord_stop_xyz_m=[
                    0	10	20	30;%x
                    1   1   1   1;%y
                    0   0   0   0]%z
                input_coord_UAV_end=[10;10;10]
                input_stop_WPT_antenna_direction_vector_xyz_m=[
                    0	0	0	0;%x
                    -1	-1	-1  -1;%y
                    0	0	0   0]%z
                
            end
            
            
            %"Simulations" codes
            
            % ######################################################################
            % ##### (1) calculate the maximum feasible WPT distance
            % ######################################################################
            
            calc_max_WPT_distance_m=km_UAV_fun_WPT_max_distance(input_frequency_Hz,input_gain_TX_dBi,input_gain_RX_dBi,input_power_UAV_charge_radiated_dBm,input_sensitivity_dBm)
            
            
            % ######################################################################
            % ##### (2) calculate distance between a stop position and a sensor
            % ######################################################################
            %calculate the distance between a stop position and a sensor
            %calc_distance_sensor_stop_m - a matrix, columns - sensors, rows - stops
            clearvars calc_distance_sensor_stop_m;
            calc_distance_sensor_stop_m=zeros(size(input_coord_stop_xyz_m,2),size(input_coord_sensor_xyz_m,2));
            for temp_idx_stop=1:size(input_coord_stop_xyz_m,2)
                for temp_idx_sensor=1:size(input_coord_sensor_xyz_m,2)
                    calc_distance_sensor_stop_m(temp_idx_stop,temp_idx_sensor)=pdist2(input_coord_stop_xyz_m(:,temp_idx_stop).',input_coord_sensor_xyz_m(:,temp_idx_sensor).');
                end
            end
            clearvars temp_idx_stop temp_idx_sensor
            
            
            % ######################################################################
            % ##### (3) calculate the nearest stop for each sensor
            % ######################################################################
            % calc_sensor_nearest_stop =
            %  Sensor | Stop
            %    1    |  1
            %    2    |  1
            %    2    |  2
            %    3    |  2
            %    4    |  3
            %    5    |  3
            %define the nearest stop for each sensor (can be more than one)
            %calc_sensor_nearest_stop structure (columns): sensor ID, stop ID
            clearvars calc_sensor_nearest_stop;
            temp_sensor_index=1
            for temp_idx_sensor=1:size(input_coord_sensor_xyz_m,2)
                clearvars temp_nearest_stops
                temp_nearest_stops=find(calc_distance_sensor_stop_m(:,temp_idx_sensor)==min(calc_distance_sensor_stop_m(:,temp_idx_sensor)));
                for temp_num_nearest_stops=1:size(temp_nearest_stops,1)%go through stops
                    %log ID of sensor
                    calc_sensor_nearest_stop(temp_sensor_index,1)=temp_idx_sensor;
                    %log ID of stop
                    calc_sensor_nearest_stop(temp_sensor_index,2)=temp_nearest_stops(temp_num_nearest_stops);
                    temp_sensor_index=temp_sensor_index+1;
                end
            end
            display(['total=' num2str(numstops) ' vs real used stops= ' num2str(length(unique(calc_sensor_nearest_stop(:,2))))])
        %    vec_effi_stops = [vec_effi_stops length(unique(calc_sensor_nearest_stop(:,2)))/numstops];
            clearvars temp_idx_sensor temp_num_nearest_stops temp_nearest_stops temp_sensor_index
            
            
            % ######################################################################
            % ##### (4) Compose the UAV's mobility schedule, total path and flight time
            % ######################################################################
            %
            % calc_schedule_mobility_drone =
            %       X         y           Z  index_id_stop  Fly start  Fly End
            %         0         0         0         0      0           0
            %         0    1.0000         0    1.0000      0.1000     120.1000
            %  10.0000    1.0000         0    2.0000     121.1000     241.1000
            %   25.0000    1.0000         0    3.0000     242.6000    362.6000
            %   30.0000         0         0         0     363.1099    363.1099
            
            %compose the UAV's mobility schedule, determine total path and flight time
            %calc_schedule_mobility_drone structure (columns): x,y,z, stop id, start_time_s, end_time_s
            clearvars calc_schedule_mobility_drone;
            clearvars temp_current_time_s temp_current_coord_xyz_m temp_next_coord_xyz_m temp_distance_m;
            temp_current_coord_xyz_m=input_coord_UAV_start;
            temp_accumulated_flight_time_s=0; % NOT USED
            temp_accumulated_flight_distance_m=0;
            temp_current_flight_time_s=0;
            temp_distance_m=0;
            calc_schedule_mobility_drone(1,1:3)=temp_current_coord_xyz_m;%coordinate
            calc_schedule_mobility_drone(1,4)=0;%invalid
            calc_schedule_mobility_drone(1,5:6)=temp_current_flight_time_s;%time
            for temp_idx_stop=1:size(input_coord_stop_xyz_m,2)
                temp_next_coord_xyz_m=input_coord_stop_xyz_m(:,temp_idx_stop);
                temp_distance_m=pdist2(temp_current_coord_xyz_m(:,:).',temp_next_coord_xyz_m(:,:).');
                temp_accumulated_flight_distance_m=temp_accumulated_flight_distance_m+temp_distance_m;
                temp_current_flight_time_s=temp_accumulated_flight_distance_m/input_speed_UAV_mps;
                calc_schedule_mobility_drone(temp_idx_stop+1,1:3)=input_coord_stop_xyz_m(:,temp_idx_stop);
                calc_schedule_mobility_drone(temp_idx_stop+1,4)=temp_idx_stop;
                calc_schedule_mobility_drone(temp_idx_stop+1,5)=temp_current_flight_time_s+(temp_idx_stop-1)*input_time_stop_duration_s;
                calc_schedule_mobility_drone(temp_idx_stop+1,6)=temp_current_flight_time_s+(temp_idx_stop)*input_time_stop_duration_s;
                temp_current_coord_xyz_m=temp_next_coord_xyz_m;
            end
            temp_distance_m=pdist2(temp_current_coord_xyz_m(:,:).',input_coord_UAV_end(:,:).');
            temp_accumulated_flight_distance_m=temp_accumulated_flight_distance_m+temp_distance_m;
            temp_current_flight_time_s=temp_accumulated_flight_distance_m/input_speed_UAV_mps;
            calc_schedule_mobility_drone(temp_idx_stop+2,1:3)=input_coord_UAV_end;
            calc_schedule_mobility_drone(temp_idx_stop+2,4)=0;
            calc_schedule_mobility_drone(temp_idx_stop+2,5)=temp_current_flight_time_s+(temp_idx_stop)*input_time_stop_duration_s;
            calc_schedule_mobility_drone(temp_idx_stop+2,6)=temp_current_flight_time_s+(temp_idx_stop)*input_time_stop_duration_s;
            result_UAV_cumulative_distance_travelled_m=temp_accumulated_flight_distance_m;
            result_UAV_cumulative_mission_time_s=calc_schedule_mobility_drone(temp_idx_stop+2,6);
            result_UAV_cumulative_interstop_travel_time_s=temp_accumulated_flight_distance_m/input_speed_UAV_mps;
            result_UAV_cumulative_stop_hower_time_s=result_UAV_cumulative_mission_time_s-result_UAV_cumulative_interstop_travel_time_s;
            clearvars temp_current_coord_xyz_m temp_next_coord_xyz_m temp_accumulated_flight_time_s temp_accumulated_flight_distance_m temp_current_flight_time_s temp_distance_m temp_idx_stop
            
            
            % ######################################################################
            % ##### (5) calculating charging schedule
            % ######################################################################
            % if drone is inside the Max WPT distance ==> the Sensor is charged
            
            %compose the "charging" schedule (can be done by a script, like below, or defined manually)
            %calc_schedule_charge structure (columns): stop ID, sensor ID,
            %start_time_s, end_time_s, charge_duration_s
            %Case 2: charge all sensors which are located closer than
            %calc_max_WPT_distance_m and direction towards which is within
            %input_directivity_UAV_antenna_WPT_degr/2 of the UAV's antenna orientation
            
            % calc_schedule_charge =
            %    Stop ID | Sensor ID | start_time_s | end_time_s | charge_duration_s
            %    1.0000    1.0000      0.1000          120.1000        120.0000
            %    2.0000    3.0000      121.1000        241.1000        120.0000
            %    3.0000    4.0000      242.6000        362.6000        120.0000
            %    3.0000    5.0000      242.6000        362.6000        120.0000
            clearvars calc_schedule_charge;
            temp_schedule_index=1;
            temp_current_time=0;
            for temp_idx_stop=1:size(input_coord_stop_xyz_m,2)%go through stops
                clearvars temp_valid_sensors
                %find valid sensor(s)
                temp_charging_time=input_time_stop_duration_s;
                temp_valid_sensors=find(calc_distance_sensor_stop_m(temp_idx_stop,:)<=calc_max_WPT_distance_m);
                for temp_num_valid_sensor=1:size(temp_valid_sensors,2)%go through sensors that are in the WPT range - and check the angle
                    temp_stop_coord_xyz_m=input_coord_stop_xyz_m(:,temp_idx_stop);
                    temp_sensor_coord_xyz_m=input_coord_sensor_xyz_m(:,temp_valid_sensors(temp_num_valid_sensor));
                    temp_sensor_direction_vector_xyz_m=temp_sensor_coord_xyz_m-temp_stop_coord_xyz_m;
                    temp_stop_WPT_antenna_direction_vector_xyz_m=input_stop_WPT_antenna_direction_vector_xyz_m(:,temp_idx_stop);
                    temp_angle_sensor_antenna_degrees = atan2(norm(cross(temp_sensor_direction_vector_xyz_m,temp_stop_WPT_antenna_direction_vector_xyz_m)), dot(temp_sensor_direction_vector_xyz_m,temp_stop_WPT_antenna_direction_vector_xyz_m)).*(180/pi);
                    if(temp_angle_sensor_antenna_degrees<input_directivity_UAV_antenna_WPT_degr/2)
                        %log ID of stop
                        calc_schedule_charge(temp_schedule_index,1)=temp_idx_stop;
                        %log IDs of sensors to charge
                        calc_schedule_charge(temp_schedule_index,2)=temp_valid_sensors(temp_num_valid_sensor);
                        %timing
                        temp_stop_starttime_s=calc_schedule_mobility_drone(find(calc_schedule_mobility_drone(:,4)==temp_idx_stop),5);
                        temp_stop_endtime_s=calc_schedule_mobility_drone(find(calc_schedule_mobility_drone(:,4)==temp_idx_stop),6);
                        temp_stop_duration_s=temp_stop_endtime_s-temp_stop_starttime_s;
                        calc_schedule_charge(temp_schedule_index,3)=temp_stop_starttime_s;
                        calc_schedule_charge(temp_schedule_index,4)=temp_stop_endtime_s;
                        calc_schedule_charge(temp_schedule_index,5)=temp_stop_duration_s;
                        %proceed
                        temp_schedule_index=temp_schedule_index+1;
                    end
                end
            end
            clearvars temp_idx_stop temp_valid_sensors temp_charging_time temp_num_valid_sensor
            clearvars temp_stop_starttime_s temp_stop_endtime_s temp_stop_duration_s
            clearvars temp_stop_coord_xyz_m temp_sensor_coord_xyz_m temp_sensor_direction_vector_xyz_m
            clearvars temp_stop_WPT_antenna_direction_vector_xyz_m temp_angle_sensor_antenna_degrees temp_schedule_index
            
            
            % ######################################################################
            % ##### (6) UAV sensor data communication capability
            % ######################################################################
            %
            % calc_schedule_communicate =
            % ===>  when input_communication_max_distance_m = 10 m
            % Sensor_id | valid_stop | stop_start_time_s | stop_endtime_s
            %   1.0000    1.0000           0.1000           120.1000
            %   2.0000    1.0000           0.1000           120.1000
            %   2.0000    2.0000           121.1000         241.1000
            %   3.0000    2.0000           121.1000         241.1000
            %   4.0000    3.0000           242.6000         362.6000
            %   5.0000    3.0000           242.6000         362.6000
            
            %compose the "UAV-sensor data communication capability" schedule
            clearvars calc_schedule_communicate calc_communicate_window_duration_s;
            %Case 1: A sensor may send its data to the UAV if the distance between the
            %sensor and the UAV is below particular threeshold
            %calc_schedule_communicate structure (columns): sensor ID, stop ID, start_time_s, end_time_s
            %calc_communicate_window_duration_s structure (columns): sensor ID,
            % ????? OBS: Here, how a sensor could transmit if it was not charged. For
            % example, in the case that Drone is near Sensor 1, the Sensor 5 does not
            % have capabilities to transmit because it does not have energy.
            % ???? The sensor only transmits until a maximum value of max distance.
            %duration_s
            temp_sensor_index=1;
            for temp_idx_sensor=1:size(input_coord_sensor_xyz_m,2)
                temp_sensor_cumulative_communication_window=0;
                clearvars temp_nearest_stops
                temp_valid_stops=find(calc_distance_sensor_stop_m(:,temp_idx_sensor)<=input_communication_max_distance_m);
                for temp_num_valid_stop=1:size(temp_valid_stops,1)%go through valid stops
                    temp_idx_valid_stop=temp_valid_stops(temp_num_valid_stop);
                    temp_stop_starttime_s=calc_schedule_mobility_drone(find(calc_schedule_mobility_drone(:,4)==temp_idx_valid_stop),5);
                    temp_stop_endtime_s=calc_schedule_mobility_drone(find(calc_schedule_mobility_drone(:,4)==temp_idx_valid_stop),6);
                    temp_stop_duration_s=temp_stop_endtime_s-temp_stop_starttime_s;
                    %log ID of sensor
                    calc_schedule_communicate(temp_sensor_index,1)=temp_idx_sensor;
                    %log ID of stop
                    calc_schedule_communicate(temp_sensor_index,2)=temp_idx_valid_stop;
                    %log start time at stop
                    calc_schedule_communicate(temp_sensor_index,3)=temp_stop_starttime_s;
                    %log end time at stop
                    calc_schedule_communicate(temp_sensor_index,4)=temp_stop_endtime_s;
                    temp_sensor_index=temp_sensor_index+1
                    temp_sensor_cumulative_communication_window=temp_sensor_cumulative_communication_window+temp_stop_duration_s;
                end
                calc_communicate_window_duration_s(temp_idx_sensor,1)=temp_idx_sensor;
                calc_communicate_window_duration_s(temp_idx_sensor,2)=temp_sensor_cumulative_communication_window;
            end
            clearvars temp_sensor_index temp_idx_sensor temp_sensor_cumulative_communication_window temp_valid_stops temp_num_valid_stop
            clearvars temp_idx_valid_stop temp_stop_starttime_s temp_stop_endtime_s temp_stop_duration_s
            
            
            % ######################################################################
            % ##### (7) Sensor energy income calculation
            % ######################################################################
            % Here is evaluated the energy that was harvest by each sensor during the
            % complete drone trip.
            %
            % result_sensors_energy_income_J =
            % TOTAL sensor_energy_collected_J
            %          0.6240    ==> Energy harvest by Sensor 1
            %             0      ==> Energy harvest by Sensor 2
            %          0.6240    ==> Energy harvest by Sensor 3
            %          0.4992    ==> Energy harvest by Sensor 4
            %          0.4992    ==> Energy harvest by Sensor 5
            
            %sensors energy income
            clearvars result_sensors_energy_budget;
            result_sensors_energy_income_J=zeros(size(input_coord_sensor_xyz_m,2),1);
            result_sensors_energy_cumulative_income_J=0
            for temp_idx_charge_session=1:size(calc_schedule_charge,1)
                temp_idx_stop=calc_schedule_charge(temp_idx_charge_session,1);
                temp_idx_sensor=calc_schedule_charge(temp_idx_charge_session,2);
                temp_calc_distance_sensor_stop_m=calc_distance_sensor_stop_m(temp_idx_stop,temp_idx_sensor);
                temp_time_charge_s=calc_schedule_charge(temp_idx_charge_session,4)-calc_schedule_charge(temp_idx_charge_session,3);
                %energy collected
                temp_attenuation_dB=km_UAV_fun_attenuation_WPT(input_frequency_Hz,input_gain_TX_dBi,input_gain_RX_dBi,temp_calc_distance_sensor_stop_m);
                % Eq. to calculate Energy harvest by sensor:
                temp_sensor_energy_collected_J=db2pow(-temp_attenuation_dB)*input_power_UAV_charge_radiated_W*temp_time_charge_s*input_efficiency_sensor_charge_percent*0.01;
                result_sensors_energy_income_J(temp_idx_sensor)=result_sensors_energy_income_J(temp_idx_sensor)+temp_sensor_energy_collected_J;
                result_sensors_energy_cumulative_income_J=result_sensors_energy_cumulative_income_J+temp_sensor_energy_collected_J;
            end
            clearvars temp_sensor_energy_collected_J temp_idx_charge_session temp_idx_stop temp_idx_sensor temp_calc_distance_sensor_stop_m temp_time_charge_s temp_energy_sensor_collected_J;
            
            
            % ######################################################################
            % ##### (8) packets transmissions are calculated
            % ######################################################################
            %packets transmissions
            %calc_packets_TX_max structure (columns): sensor ID, max_packets_energy
            % ######### Pacotes calculados usando a quantidade de energia que se
            % necesita para transmitir cada pacote
            %
            %  calc_packets_TX_max =
            %Sensor ID |  Packets transmitted
            %     1       31
            %     2        0
            %     3       31
            %     4       24
            %     5       24
            result_sensors_cumulative_packets_sent=0;
            for temp_idx_sensor=1:size(input_coord_sensor_xyz_m,2)
                calc_packets_TX_max(temp_idx_sensor,1)=temp_idx_sensor;
                calc_packets_TX_max(temp_idx_sensor,2)=floor((result_sensors_energy_income_J(temp_idx_sensor,1)+input_energy_sensor_buffer_J(temp_idx_sensor))/(input_energy_sensor_sensing_J+input_energy_sensor_report_J));
                result_sensors_cumulative_packets_sent=result_sensors_cumulative_packets_sent+calc_packets_TX_max(temp_idx_sensor,2);
            end
            clearvars temp_idx_sensor
            
            
            % ######################################################################
            % ##### (9) calculate the UAV energy consumption
            % ######################################################################
            %UAV energy consumption
            clearvars result_UAV_energy_total result_UAV_energy_travel_J result_UAV_energy_hower_J result_UAV_energy_communicate result_UAV_energy_sensors_charge;
            result_UAV_energy_travel_J=result_UAV_cumulative_interstop_travel_time_s*input_power_UAV_hower_W;
            result_UAV_energy_hower_J=result_UAV_cumulative_stop_hower_time_s*input_power_UAV_hower_W;
            result_UAV_energy_communicate_J=result_UAV_cumulative_stop_hower_time_s*input_power_UAV_receive_constant_W+input_energy_UAV_per_event_J*result_sensors_cumulative_packets_sent;
            result_UAV_energy_sensors_charge_J=result_UAV_cumulative_stop_hower_time_s*input_power_UAV_charge_radiated_W*(100/input_efficiency_UAV_charge_percent);
            result_UAV_energy_total_J=result_UAV_energy_sensors_charge_J+result_UAV_energy_communicate_J+result_UAV_energy_hower_J+result_UAV_energy_travel_J;
            result_UAV_energy_consumed_percent=result_UAV_energy_total_J/input_energy_UAV_buffer_volume_J*100
            
            if input_energy_UAV_buffer_volume_J<result_UAV_energy_total_J
                fprintf(2,'WARNING: ENERGY INCOSISTENCY - UAV energy buffer is only %e J, while mission requires %e J',input_energy_UAV_buffer_volume_J,result_UAV_energy_total_J);
                break;
            end
            
            % Added by Dick CM
            number_stops = [number_stops size(input_coord_stop_xyz_m,2)];

            % Calculating the sum of packages that were transmitted
            sum_packetsTx = [sum_packetsTx sum(calc_packets_TX_max(:,2))];
            
            % Calculating the # of sensors that unless transmit 1 package
            numSensorsTxpackages_vector = [numSensorsTxpackages_vector length(find(calc_packets_TX_max(:,2)>0))] ;
            
            % Calculating the Transmition efficiency
            eff_tx_packages_vector = [eff_tx_packages_vector sum(calc_packets_TX_max(:,2))/result_UAV_energy_total_J];
            
            % Calculating the rate of drones usability
            vec_effi_stops = [vec_effi_stops length(unique(calc_sensor_nearest_stop(:,2)))/numstops];
            
            display(['numstops=' num2str(numstops)])
            
        end
        clearvars  calc_packets_TX_max
    
        figure(3)
        plot_zoom =plot_zoom_vec(jj);
        if plot_zoom
            colororder(C);
            if only_once == 0
                axes('position',[.18 .35 .25 .25])
                only_once =only_once+1;
                box on
            end
            plot(number_stops,eff_tx_packages_vector,'-.','LineWidth',3)
            hold on
        else
            hold on
            plot(number_stops,eff_tx_packages_vector,lineplot_vec(jj,:),'LineWidth',3)
        end

        
    end

    
    figure(3)
    if plot_zoom
        axis([12 18 1.8e-3 2.15e-3])
    end
    if ~plot_zoom
        legend('T = 20S - S2 (Cluster)', 'T = 25S - S2 (Cluster)',...
            'T = 27S - S2 (Cluster)','T = 70S - S2 (Cluster)', ...
            'T = 20S - S1', 'T = 25S - S1', ...
            'T = 27S - S1','T = 70S - S1')
        xlabel("Number of UAV 'stop-points'")
        ylabel('Efficiency (packets/J)')
      %  title('Sum of packages_{tx}/Total energy consumed by UAVs ')
        grid on
        set(gca,'fontsize',10,'FontWeight','bold')
    end
end

grid on

title(' ')
