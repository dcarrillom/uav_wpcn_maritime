% Function generate randomly drone stops
function [matrix_stop_positions, matrix_stop_ant_direction] = ...
    factory_drone_stops(num_stops,...
    initial_point,...
    final_point,...
    method)
if method == 'equal__separated'
    x_values = linspace(initial_point, final_point, num_stops);
    y_values = ones(1,length(x_values));
    z_values = zeros(1,length(x_values));
    
    matrix_stop_positions = [x_values;... %x
        y_values;... %y
        z_values];  %z
    
    x_ant_direction = zeros(1,length(x_values));
    y_ant_direction = ones(1,length(y_values)) * -1; % Direction to the axe Y negative
    z_ant_direction = zeros(1,length(x_values));
    
    matrix_stop_ant_direction = [x_ant_direction;... %x
        y_ant_direction;... %y
        z_ant_direction];  %z
elseif method == 'random_separated'
    % based on initial_point and final_point choose randomly the
    % positions of the num_stops
    % only for x axis in this version
    x_values = [initial_point sort(floor(...
        (final_point-initial_point)*rand(1,num_stops-2)+initial_point))...
        final_point];
    y_values = ones(1,length(x_values));
    z_values = zeros(1,length(x_values));
    
    matrix_stop_positions = [x_values;... %x
        y_values;... %y
        z_values];  %z
    
    x_ant_direction = zeros(1,length(x_values));
    y_ant_direction = ones(1,length(y_values)) * -1; % Direction to the axe Y negative
    z_ant_direction = zeros(1,length(x_values));
    
    matrix_stop_ant_direction = [x_ant_direction;... %x
        y_ant_direction;... %y
        z_ant_direction];  %z
    
else
    % This scenario is not necessary for Drone Stops
    Display('need to be developed when 2 0r 3 form a cluster...')
    
    
end
end