function attenuation_dB = km_UAV_fun_attenuation_WPT(frequency_Hz,gain_TX_dBi,gain_RX_dBi,distance_m)
    attenuation_FSPL_dB=mag2db(4*pi*distance_m*frequency_Hz/physconst('LightSpeed'))-gain_TX_dBi-gain_RX_dBi;%good-old free space path loss
    attenuation_dB=attenuation_FSPL_dB;
end