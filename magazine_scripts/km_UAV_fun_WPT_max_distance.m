function distance_m = km_UAV_fun_WPT_max_distance(frequency_Hz,gain_TX_dBi,gain_RX_dBi,P_TX_dBm,sensitivity_dBm)
    max_attenuation_FSPL_dB = P_TX_dBm+gain_RX_dBi+gain_TX_dBi-sensitivity_dBm;
    distance_m=db2mag(max_attenuation_FSPL_dB)*physconst('LightSpeed')/(4*pi*frequency_Hz);
end